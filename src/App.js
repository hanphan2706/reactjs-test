import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Authentication, AuthProvider } from './auth';
import Home from './containers/Home';
import Login from './containers/Login';

const App = () => {
  return (
    <AuthProvider>
      <Router>
        <Authentication>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/login" component={Login} />
          </Switch>
        </Authentication>
      </Router>
    </AuthProvider>
  );
};

export default App;
