export const USER = {
  id: '1',
  username: 'a',
  password: 'a',
  fullName: 'Kittie'
};

const sources = [
  'https://ourfitpets.com/wp-content/uploads/2018/02/pexels-photo-399647-400x400.jpeg',
  'https://www.peta.org.au/wp-content/uploads/orange-cat-400x400.jpg',
  'https://www.catsforafrica.co.za/wp-content/uploads/2017/05/CofA-BFCat-Walking-Square-400x400-300x300.jpg',
  'https://i.pinimg.com/474x/6d/15/54/6d1554e2aca033d9c6f476467516b8ec.jpg',
  'https://www.cats.org.uk/media/1500/our-strategy-cover-image.jpg?width=400'
];

const generateImages = () => {
  const images = [];
  for (let i = 1; i <= 105; i++) {
    images.push({
      id: i,
      src: sources[Math.floor(Math.random() * 5)]
    });
  }
  return images;
};

export const IMAGES = generateImages();
