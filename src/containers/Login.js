import React, { useState, useContext } from 'react';
import {
  Button,
  Form,
  Grid,
  Header,
  Segment,
  Message
} from 'semantic-ui-react';
import { AuthContext, AuthActions } from '../auth';
import { USER } from '../shared/data';

const Login = () => {
  const { dispatch } = useContext(AuthContext);
  const [user, setUser] = useState({
    username: '',
    password: ''
  });
  const [formError, setFormError] = useState(false);

  const handleChange = event => {
    const a = { ...user, [event.target.name]: event.target.value };
    setUser(a);
  };

  const submitLogin = () => {
    if (user.username === USER.username && user.password === USER.password) {
      dispatch(AuthActions.setUserData(USER));
    } else {
      setFormError(true);
    }
  };

  const canSubmit = () => user.username.length > 0 && user.password.length > 0;

  return (
    <Grid
      textAlign="center"
      style={{ height: '100vh', padding: 16 }}
      verticalAlign="middle"
    >
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" textAlign="center">
          Log-in to your account
        </Header>
        <Form size="large" error={formError}>
          <Segment stacked>
            <Message
              error
              content="Your account or password is incorrect, please try again!"
            />
            <Form.Input
              fluid
              icon="user"
              iconPosition="left"
              placeholder="Username"
              name="username"
              onChange={handleChange}
              required
            />
            <Form.Input
              fluid
              icon="lock"
              iconPosition="left"
              placeholder="Password"
              type="password"
              name="password"
              onChange={handleChange}
              required
            />
            <Button
              type="button"
              onClick={submitLogin}
              fluid
              size="large"
              disabled={!canSubmit()}
            >
              Login
            </Button>
          </Segment>
        </Form>
      </Grid.Column>
    </Grid>
  );
};

export default Login;
