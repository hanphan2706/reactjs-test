import React, { useContext } from 'react';
import { AuthContext } from '../auth';
import ResponsiveContainer from '../components/layout/ResponsiveContainer';
import UserProfile from '../components/user/UserProfile';

const Home = () => {
  const { state: user } = useContext(AuthContext);

  return (
    <ResponsiveContainer>
      <UserProfile user={user} />
    </ResponsiveContainer>
  );
};

export default Home;
