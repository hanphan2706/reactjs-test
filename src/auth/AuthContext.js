import { createContext } from 'react';
import { authInitialState } from './store/reducer';

const AuthContext = createContext(authInitialState);

export default AuthContext;
