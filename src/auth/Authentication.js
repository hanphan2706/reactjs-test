import React, { useContext, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import AuthContext from './AuthContext';

const Authentication = ({ history, children }) => {
  const { state } = useContext(AuthContext);

  useEffect(() => {
    const pathname = state.id ? '/' : 'login';
    history.push({
      pathname
    });
  }, [state]);

  return <>{children}</>;
};

export default withRouter(Authentication);
