import React, { useReducer } from 'react';
import AuthContext from './AuthContext';
import { authReducer, authInitialState } from './store/reducer';

const AuthProvider = ({ children }) => {
  const [state, dispatch] = useReducer(authReducer, authInitialState);
  return (
    <AuthContext.Provider
      value={{
        state,
        dispatch
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export default AuthProvider;
