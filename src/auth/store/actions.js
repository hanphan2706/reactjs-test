export const SET_USER_DATA = 'SET_USER_DATA';
export const USER_LOGGED_OUT = 'USER_LOGGED_OUT';

export const setUserData = user => ({
  type: SET_USER_DATA,
  payload: user
});

export const logoutUser = () => ({
  type: USER_LOGGED_OUT
});
