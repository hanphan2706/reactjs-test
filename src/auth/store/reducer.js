import { SET_USER_DATA, USER_LOGGED_OUT } from './actions';

export const authInitialState = {};

export const authReducer = (state = authInitialState, action) => {
  switch (action.type) {
    case SET_USER_DATA: {
      return { ...authInitialState, ...action.payload };
    }
    case USER_LOGGED_OUT:
      return authInitialState;
    default: {
      return state;
    }
  }
};
