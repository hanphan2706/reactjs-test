import React from 'react';
import PropTypes from 'prop-types';
import { Item } from 'semantic-ui-react';
import GalleryModal from '../modal/Gallery';

const UserProfile = ({ user }) => (
  <div style={{ padding: 24 }}>
    <Item.Group>
      <Item>
        <Item.Image
          size="small"
          src="https://www.cats.org.uk/media/1500/our-strategy-cover-image.jpg?width=400"
        />
        <Item.Content>
          <Item.Header as="a">{user.fullName}</Item.Header>
          <Item.Meta>18-year-old cat</Item.Meta>
          <Item.Description>
            <p>
              young and pretty.
              <br />
              good taking selfies.
            </p>
            <GalleryModal />
          </Item.Description>
        </Item.Content>
      </Item>
    </Item.Group>
  </div>
);

UserProfile.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.string.isRequired,
    username: PropTypes.string.isRequired,
    password: PropTypes.string.isRequired,
    fullName: PropTypes.string.isRequired
  }).isRequired
};

export default UserProfile;
