import React, { useState } from 'react';
import { Button, Image, Modal, Visibility } from 'semantic-ui-react';
import * as Utils from '../../shared/utils';
import { IMAGES } from '../../shared/data';

const LIMIT_PER_PAGE = 25;

const GalleryModal = () => {
  const isMobile = Utils.detectMobile();
  const [images, setImages] = useState(IMAGES.slice(0, LIMIT_PER_PAGE));
  const onBottomVisible = () => {
    setImages(IMAGES.slice(0, images.length + LIMIT_PER_PAGE));
  };

  return (
    <Modal closeIcon trigger={<Button>WATCH FOR FREE</Button>}>
      <Modal.Header>My photos</Modal.Header>
      <Modal.Content scrolling style={{ height: 400 }}>
        <Visibility
          style={{ display: 'flex', flexWrap: 'wrap' }}
          continuous={false}
          once={false}
          onBottomVisible={onBottomVisible}
        >
          {images.map(img =>
            isMobile ? (
              <Image key={img.id} src={img.src} style={{ padding: 8 }} />
            ) : (
              <div key={img.id} style={{ flex: '20%', padding: 8 }}>
                <Image src={img.src} />
              </div>
            )
          )}
        </Visibility>
      </Modal.Content>
    </Modal>
  );
};

export default GalleryModal;
