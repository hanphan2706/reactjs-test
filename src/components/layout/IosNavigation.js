import React from 'react';
import { Container, Icon, Menu, Visibility } from 'semantic-ui-react';

const MenuItem = ({ title, iconName, active }) => {
  return (
    <Menu.Item
      style={{ display: 'flex', flexDirection: 'column' }}
      active={active}
    >
      <Icon name={iconName} style={{ marginBottom: 4 }} />
      {title}
    </Menu.Item>
  );
};

const IosNavigation = ({ children }) => {
  return (
    <Container
      textAlign="center"
      style={{ height: '100vh', padding: '1em 0em' }}
    >
      <Visibility>{children}</Visibility>
      <Menu secondary fixed={'bottom'} inverted={false} fluid widths={4}>
        <MenuItem title="Home" iconName="home" active />
        <MenuItem title="Notifications" iconName="box" />
        <MenuItem title="Settings" iconName="setting" />
        <MenuItem title="Logout" iconName="sign-out" />
      </Menu>
    </Container>
  );
};

export default IosNavigation;
