import React from 'react';
import { Responsive, Sidebar } from 'semantic-ui-react';
import * as Utils from '../../shared/utils';
import IosNavigation from './IosNavigation';
import AndroidNavigation from './AndroidNavigation';

const getWidth = () => {
  const isSSR = typeof window === 'undefined';
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

const Navigator = Utils.detectIos() ? IosNavigation : AndroidNavigation;

const MobileContainer = ({ children }) => {
  return (
    <Responsive
      as={Sidebar.Pushable}
      getWidth={getWidth}
      maxWidth={Responsive.onlyMobile.maxWidth}
    >
      <Navigator>{children}</Navigator>
    </Responsive>
  );
};

export default MobileContainer;
