import React from 'react';
import { Container, Icon, Menu, Visibility } from 'semantic-ui-react';

const AndroidNavigation = ({ children }) => {
  return (
    <Container
      textAlign="center"
      style={{ height: '100vh', padding: '1em 0em' }}
    >
      <Visibility>{children}</Visibility>
      <Menu pointing secondary fixed={'top'} fluid widths={4}>
        <Menu.Item active>
          <Icon name="home" />
        </Menu.Item>
        <Menu.Item>
          <Icon name="box" disabled />
        </Menu.Item>
        <Menu.Item>
          <Icon name="setting" disabled />
        </Menu.Item>
        <Menu.Item>
          <Icon name="sign-out" disabled />
        </Menu.Item>
      </Menu>
    </Container>
  );
};

export default AndroidNavigation;
